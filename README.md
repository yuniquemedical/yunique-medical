Yunique Medical specializes in turning your individual clock back so your future is bright, healthy and renewed. We use multiple anti-aging modalities including bio-identical hormone pellets, testosterone replacement, growth hormone therapy, peptides, nutrient injections and functional medicine.
||
Address: 3629 Co Rd 466, Oxford, FL 34484, USA || 
Phone: 352-209-4249
